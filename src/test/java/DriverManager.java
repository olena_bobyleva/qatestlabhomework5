import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class DriverManager {


    public static WebDriver getDriver(String browser, String urlRemote, String device) {
        String osName = OsValidator.OSName();
        if (osName.equals("win") || osName.equals("unix")) {
            browser = browser.concat("-".concat(osName));
        }
        System.out.println(browser);
        WebDriver b = null;
        switch (browser) {
            case "firefox-win":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver.exe").getFile()).getPath());
                b = new FirefoxDriver();
                break;
            case "firefox-unix":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver").getFile()).getPath());
                b = new FirefoxDriver();
                break;
            case "remote-firefox-win":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver.exe").getFile()).getPath());
                FirefoxOptions optionsFirefoxRemoteWin = new FirefoxOptions();
                optionsFirefoxRemoteWin.addArguments("--headless");
                try {
                    b = new RemoteWebDriver(new URL(urlRemote),optionsFirefoxRemoteWin);
                }catch (MalformedURLException e){
                    e.printStackTrace();
                }
                break;
            case "remote-firefox-unix":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(DriverManager.class.getResource("/geckodriver").getFile()).getPath());
                FirefoxOptions optionsFirefoxRemoteUnix = new FirefoxOptions();
                optionsFirefoxRemoteUnix.addArguments("--headless");
                try {
                    b = new RemoteWebDriver(new URL(urlRemote),optionsFirefoxRemoteUnix);
                }catch (MalformedURLException e){
                    e.printStackTrace();
                }
                break;
            case "chrome-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver.exe").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "chrome-unix":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "remote-chrome-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver.exe").getFile()).getPath());
                ChromeOptions optionsChromeRemoteWin = new ChromeOptions();
                optionsChromeRemoteWin.addArguments("--headless");
                try {
                    b = new RemoteWebDriver(new URL(urlRemote),optionsChromeRemoteWin);
                }catch (MalformedURLException e){
                    e.printStackTrace();
                }
                break;
            case "remote-chrome-unix":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                ChromeOptions optionsChromeRemoteUnix = new ChromeOptions();
                optionsChromeRemoteUnix.addArguments("--headless");
                try {
                    b = new RemoteWebDriver(new URL(urlRemote),optionsChromeRemoteUnix);
                }catch (MalformedURLException e){
                    e.printStackTrace();
                }
                break;
            case "ie-win":
            case "internet explorer-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                b = new ChromeDriver();
                break;
            case "ie-unix":
            case "internet explorer-unix":
                System.out.println("The Internet Explorer browser is not supported in this operation system");
                break;
            case "mobile-chrome-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver.exe").getFile()).getPath());
                Map<String,String>mobileEmulation = new HashMap<>();
                mobileEmulation.put("deviceName",device);
                ChromeOptions chromeOptionsMobileWin = new ChromeOptions();
                chromeOptionsMobileWin.setExperimentalOption("mobileEmulation",mobileEmulation);
                b = new ChromeDriver(chromeOptionsMobileWin);
                break;
            case "mobile-chrome-unix":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                Map<String,String>mobileEmulationUnix = new HashMap<>();
                mobileEmulationUnix.put("deviceName",device);
                ChromeOptions chromeOptionsMobileUnix = new ChromeOptions();
                chromeOptionsMobileUnix.setExperimentalOption("mobileEmulation",mobileEmulationUnix);
                b = new ChromeDriver(chromeOptionsMobileUnix);
                break;
            case "remote-mobile-chrome-win":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver.exe").getFile()).getPath());
                Map<String,String>mobileEmulationRemoteWin = new HashMap<>();
                mobileEmulationRemoteWin.put("deviceName",device);
                ChromeOptions chromeOptionsMobileRemoteWin = new ChromeOptions();
                chromeOptionsMobileRemoteWin.addArguments("--headless");
                chromeOptionsMobileRemoteWin.setExperimentalOption("mobileEmulation",mobileEmulationRemoteWin);
                try {
                b = new RemoteWebDriver(new URL(urlRemote),chromeOptionsMobileRemoteWin);
            }catch (MalformedURLException e){
                e.printStackTrace();
            }
                break;
            case "remote-mobile-chrome-unix":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(DriverManager.class.getResource("/chromedriver").getFile()).getPath());
                Map<String,String>mobileEmulationRemoteUnix = new HashMap<>();
                mobileEmulationRemoteUnix.put("deviceName",device);
                ChromeOptions chromeOptionsMobileRemoteUnix = new ChromeOptions();
                chromeOptionsMobileRemoteUnix.addArguments("--headless");
                chromeOptionsMobileRemoteUnix.setExperimentalOption("mobileEmulation",mobileEmulationRemoteUnix);
                try {
                    b = new RemoteWebDriver(new URL(urlRemote),chromeOptionsMobileRemoteUnix);
                }catch (MalformedURLException e){
                    e.printStackTrace();
                }
                break;
                default:
                b = new ChromeDriver();
        }
        return b;
    }

    public static EventFiringWebDriver getConfigureDriver(String browser, String urlRemote, String device) {
        EventFiringWebDriver driver = new EventFiringWebDriver(getDriver(browser,urlRemote, device));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;
    }

}
                                           