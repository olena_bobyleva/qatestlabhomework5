public class OsValidator {

    private static String OS = System.getProperty("os.name").toLowerCase();

    public static String OSName() {
        String s = "";
        if (isWindows()) {
            System.out.println("This is Windows");
            s = "win";
        } else if (isMac()) {
            System.out.println("This is Mac");
            s = "macos";
        } else if (isUnix()) {
            System.out.println("This is Unix or Linux");
            s = "unix";
        } else if (isSolaris()) {
            System.out.println("This is Solaris");
            s = "sunos";
        } else {
            System.out.println("Your OS is not support!!");
        }
        return s;
    }

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {

        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {

        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

    }

    public static boolean isSolaris() {

        return (OS.indexOf("sunos") >= 0);

    }

}