import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;


import java.util.List;
import java.util.Random;

public class TestOrderOneProduct {
    public static final String URL2 = "http://prestashop-automation.qatestlab.com.ua/";
    public String productUrl;
    public WebDriver driver;
    public WebDriverWait wait;
    Random random = new Random();
    String productName;
    int productQuantity;
    float productPrice;
    int orderQuantity = 1;
    String platform;


    @BeforeTest
    @Parameters({"browser", "urlremote", "platform", "device"})
    public void setUp(String browser, @Optional String urlRemote,
                      @Optional String platformParameter, @Optional String device) {
        log("Preparing the WebDriver " + browser);
        if (urlRemote == null) {
            urlRemote = "";
        }
        if (platformParameter == null) {
            platform = "desktop";
        } else platform = platformParameter;
        if (device == null) {
            device = "iPhone 6/7/8";
        }
        try {
            driver = DriverManager.getConfigureDriver(browser, urlRemote, device);
            driver.manage().window().maximize();
            wait = new WebDriverWait(driver, 15);
        } catch (NullPointerException e) {
            log("The WebDriver is not launched");
            e.printStackTrace();
        }
    }

    @AfterTest
    public void tearDown() {
        log("The test is finished");
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void prepareEnvironment() {
        System.out.println("Next test is starting");
    }

    @AfterMethod
    public void clearEnvironment() {
        System.out.println("The test has finished");
    }

    @Test
    public void testCheckSiteVersion() {
        Assert.assertTrue(driver != null, "The WebDriver is not launched");
        try {
            log("Navigate to Home-page of the site: " + URL2);
            driver.navigate().to(URL2);
            System.out.println("platform = " + platform);
            log("The platform is " + platform);
            siteVersion(platform);
        } catch (NullPointerException e) {
            log("The test is skipped with Exception " + e);
            e.printStackTrace();
        }
    }


    @Test
    public void testOrder() {
        log("Search the product with entered data");
        searchProductOnPage();
        addToCart();
        checkProductAttributtesInCart();
        inputDataCheckOut();
        checkOrder();
        driver.navigate().to(productUrl);
        checkProductQuantity();
    }


    private void siteVersion(String platform) {
        log("Check the site version");
        WebElement mobilePanel = driver.findElement(By.cssSelector(".hidden-md-up.text-xs-center.mobile"));
        if (mobilePanel.isDisplayed()) {
            log("The mobile version of site is displayed");
        } else {
            log("The desktop version of site is displayed");
        }
        if (platform.equals("mobile")) {
            Assert.assertTrue(mobilePanel.isDisplayed(), "This is not mobile version");
        } else Assert.assertFalse(mobilePanel.isDisplayed(), "This is not desktop varsion");
    }

    private void searchProductOnPage() {

        waitingByClassName("all-product-link");
        driver.findElement(By.className("all-product-link")).click();
        waitingByClassName("product-description");
        List<WebElement> products = driver.findElements(By.cssSelector(".h3.product-title > a"));
        Random random = new Random();
        int i = random.nextInt(products.size());
//        Select random product
        products.get(i).click();
    }

    private void addToCart() {
        waitingByClassName("footer-container");
//        Reading the displayed product attributes
        productUrl = driver.getCurrentUrl();
        productName = driver.findElement(By.className("h1")).getText().toLowerCase();
        String priceOnProductPage = driver.findElement(By.className("current-price")).getText();
        productPrice = priceToFloat(priceOnProductPage);
        driver.findElement(By.cssSelector("a[href='#product-details']")).click();
        waitingByClassName("product-quantities");
        String quantityOnProductPage = driver.findElement(By.cssSelector(".product-quantities>span")).getText();
        productQuantity = quantityToInt(quantityOnProductPage);
        log("Read product attributtes");
        log("The  product name: " + productName);
        log("The product price: " + priceOnProductPage);
        log("The actual quantity: " + quantityOnProductPage);

//        Add product to the cart
        WebElement inputQuantity = driver.findElement(By.id("quantity_wanted"));
        inputQuantity.sendKeys(org.openqa.selenium.Keys.BACK_SPACE);
        inputQuantity.sendKeys(Integer.toString(orderQuantity));
        driver.findElement(By.cssSelector(".btn.btn-primary.add-to-cart")).click();

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(
                By.className("cart-content"))));
        driver.findElement(By.cssSelector(".cart-content > .btn.btn-primary")).click();
    }

    private void checkProductAttributtesInCart() {
        waitingByClassName("product-line-grid-body");
        String productNameInCart = driver.findElement(By.className("label"))
                .getText().toLowerCase();
        WebElement quantityProductInCart = driver.findElement(
                By.className("js-cart-line-product-quantity"));
        String quantity = quantityProductInCart.getAttribute("value");
        int quantityInCartToInt = Integer.valueOf(quantity);
        String productPriceInCart = driver.findElement(By.className("product-price")).getText();
        float productPriceInCartToFloat = priceToFloat(productPriceInCart);
        log("The product attributtes in the Cart:");
        log("Product name " + productNameInCart);
        log("Quantity: " + quantity);
        log("Price: " + productPriceInCartToFloat);
        Assert.assertEquals(productNameInCart, productName,
                "The product name in the cart and on the product page are not match");
        Assert.assertEquals(quantityInCartToInt, orderQuantity,
                "The product quantity in the cart is not match to the ordered quantity");
        Assert.assertEquals(productPriceInCartToFloat, productPrice,
                "The product price in the cart and on the product page are not match");
        driver.findElement(By.cssSelector(".checkout a")).click();

    }

    private void inputDataCheckOut() {
        waitingByName("firstname");
        driver.findElement(By.name("firstname")).sendKeys("Test");
        driver.findElement(By.name("lastname")).sendKeys("Test");
        driver.findElement(By.name("email")).sendKeys(randomEmail());
        driver.findElement(By.name("continue")).click();
        waitingByName("address1");
        driver.findElement(By.name("address1")).sendKeys("Test");
        driver.findElement(By.name("postcode")).sendKeys("50000");
        driver.findElement(By.name("city")).sendKeys("Test");
        driver.findElement(By.name("confirm-addresses")).click();
        waitingByName("confirmDeliveryOption");
        driver.findElement(By.name("confirmDeliveryOption")).click();
        waitingByClassName("payment-options");
        driver.findElement(By.xpath("//*[@id='payment-option-1']")).click();
        driver.findElement(By.xpath("//*[@id='conditions_to_approve[terms-and-conditions]']")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='payment-confirmation']//button")));
        driver.findElement(By.xpath("//*[@id='payment-confirmation']//button")).click();
    }

    private void checkOrder() {
        String orderedProductName = driver.findElement(
                By.cssSelector("#order-items div.col-sm-4.col-xs-9.details")).getText().toLowerCase();
        orderedProductName = orderedProductName.substring(0, productName.length());
        String orderedQuantity = driver.findElement(
                By.cssSelector("#order-items div.col-xs-2")).getText();
        String priceOfOrderedProduct = driver.findElement(
                By.cssSelector("#order-items div.col-xs-5.text-sm-right.text-xs-left")).getText();
        int quantityInOrder = Integer.valueOf(orderedQuantity);

        float priceOfOrderedProductToFloat = priceToFloat(priceOfOrderedProduct);
        log("The product attributtes in the Cart:");
        log("Product name " + orderedProductName);
        log("Quantity: " + orderedQuantity);
        log("Price: " + priceOfOrderedProduct);
        Assert.assertEquals(orderedProductName, productName,
                "The product name in the order and on the product page are not match");
        Assert.assertEquals(quantityInOrder, orderQuantity,
                "The product quantity in the cart is not match to the ordered quantity");
        Assert.assertEquals(priceOfOrderedProductToFloat, productPrice,
                "The product price in the cart and on the product page are not match");

    }

    private void checkProductQuantity() {
        waitingByClassName("footer-container");
//        Reading the displayed product attributes
        driver.findElement(By.cssSelector("a[href='#product-details']")).click();
        waitingByClassName("product-quantities");
        String quantityOnProductPage = driver.findElement(
                By.cssSelector(".product-quantities>span")).getText();
        int newProductQuantity = quantityToInt(quantityOnProductPage);
        log("Old product quantity: "+productQuantity);
        log("Ordered quantity: "+orderQuantity);
        log("Current product quantity: "+quantityOnProductPage);
        Assert.assertEquals(newProductQuantity,productQuantity-orderQuantity,
                "The new quantity is not correct");
    }


    private String randomEmail() {
        Random random = new Random();
        String s = "testing" + random.nextInt(1000) + "@gmail.com";
        return s;
    }

    private String randomString() {
        Random random = new Random();
        String s = "Test" + random.nextInt(1000);
        return s;
    }

    private int quantityToInt(String quantityString) {
        quantityString = quantityString.substring(0, quantityString.indexOf(" "));
        int q = Integer.valueOf(quantityString);
        return q;
    }

    private float priceToFloat(String priceString) {
        priceString = priceString.substring(0, priceString.indexOf(" ")).replace(",", ".");
        float p = Float.valueOf(priceString);
        return p;
    }


    public void waitingByClassName(String className) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.className(className)));
    }

    public void waitingById(String id) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

    private void waitingByName(String name) {
        wait.withMessage("The finding block is not found")
                .until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
    }

    private void log(String message) {
        Reporter.log(message);
    }


}
